# README #

This readme contains information about the how to use a snakemake file to perform several
bioinformatics tools on a genome file(in this case of macaca malutta). Please ensure the 
follow the provided steps in the correct order, or the snakemake file may not function properly.


## Installation
The very first thing that needs to be installed is snakemake. The snakemake workflow management
system is a useful tool to create reproducible and scalable data analyses. These workflows are 
described in a Python based language. 
First things first: be sure to download conda. This can be done [here](https://www.anaconda.com/distribution/)
Once this is installed, open the anaconda terminal and copy the following link in the directory of your choice.

```bash
$ conda install -c conda-forge -c bioconda snakemake
```
With this, snakemake is now installed.

Other than this the used data analysis tools are needed. The tools used is this workflow, in order, are:

1. bowtie2-build v2.3.4.3
2. fastqc v0.11.8
3. bowtie2 v2.3.4.3
4. picard sort v2.18.25
5. picard fixmate v2.18.25
6. picard mark duplicates 2.18.25
7. sam sort v1.9
8. feature counts v1.6.3

In addition both python and R are required.
Python v3.7.3 was used, and R v3.5.2 was used.

After this the output of feature counts will be used to produce a heatmap, using R.

Bowtie2-build is part of the bowtie2 tool and uses the input file to create indexes, which are to be used in bowtie2.
In order to install this tool copy the following command:
```bash
$ conda install bowtie2
```
Fastqc provided a simple way to do some quality control checks on raw sequence data.
It provides a modular set of analyses which you can use to give a quick impression of 
whether your data has any problems of which you should be aware before doing any further analysis.
Fastqc can be installed in the following way:

```bash
$ conda install -c bioconda fastqc
```

bowtie2 is used to allign squence reads to long reference sequences. This tool is already installed.  

Picard requires java to work, so begin by downloading and installing [java](https://www.java.com/nl/download/).
After this download the latest picard version [here](https://github.com/broadinstitute/picard/releases).
Then open the downloaded package and place the folder containing the jar file in a convenient directory on your hard drive (or server)
This test whether picard can be run, use the following command:

```bash
$ java -jar /path/to/picard.jar -h 
```

Picard sort sort the input into either sam or bam files by a argument. In case of this workflow the output will generate 
bam files and the used argument is queryname. This output is then used by picard fixmate. This tool verifies
mate-pair information and fixes them if needed. This tool ensures that all mate-pair information is in sync
between each read and its mate pair. The final picard tool is mark duplicates. Mark duplicates locates and tags duplicate
reads(duplicate reads are defined as originating from a single fragment of DNA). 


Samtools sort can be downloaded [here](http://www.htslib.org/download/). This page also contains information
on how to install it. Please be sure to follow these instructions.

The final tool that needs to be installed is called feature counts. Feature counts can be downloaded 
[here](http://bioinf.wehi.edu.au/subread-package/).
First, this file needs to be decompressed. This can be achieved by executing the following command:

```bash
tar zxvf subread-1.x.x.tar.gz
```

Enter the src subdirectory under the home directory of the package and then 
issue the following command to build it on a Linux/unix computer:
```bash
make -f Makefile.Linux
```

Finally, R is used to create the heatmaps. R can be downloaded [here](https://www.r-project.org/).


## Selecting input and output directories
The smk files themselves don't require a manual definition of any output or input directories. This 
may only be changed in the provided config.yaml file. 
Also ensure that the variable names are not changed whatsoever, because changing this will cause the script to crash.
In addition, change genome variable in the config.yaml to match the location of the genome 
you want to run this pipeline on and change the locations to match your preference.
The Rscript doesn't require manual input either, so be sure not to change anything in this script.

The rule build_indexes has been commented out in the individual snakemake files for the two cohort.
This has been done to avoid rule naming conflicts. If you are interested in only 1 cohort, please uncomment
this rule and run either SnakeNed.smk or SnakeBra.smk, depending on your interest.


## Running the script
In order to run the script, use the terminal to go to the location where snakemake is installed. Then run 
the following command.

```bash
snakemake --snakefile /path/to/filename.smk
```

This will automatically run the entire pipeline.

# Author

Rik van de Pol  
Bioinformatics Student  
05-04-2020