configfile: "config.yaml"
import glob
import os
from os.path import join

samplesNed = [os.path.basename(x) for x in glob.glob(config["neddata"] + "*")]
samplesBasenameNed = [x.split(".")[0] for x in samplesNed]




rule all_ned:
    input:
         expand(config["heatmapned"])
    message:
        "created {input}"



'''
Build the indexes to be used in bowtie2
'''
# rule build_indexes:
#     input:
#         config["genome"]
#     threads: 80
#     log:
#         join(config["loggingned"], "indexes/index.log")
#     output:
#         index = expand(config["bow_index"] + ".{ind}.bt2", ind=range(1,5)),
#         indexrev = expand(config["bow_index"] + ".rev.{ind}.bt2", ind=range(1,3))
#     shell:
#         "(bowtie2-build {input} {config[bow_index]} --threads {threads}) 2>> {log}"



def nedfilefinder(basename):
    """
    Find the provided file in the filesystem
    """
    file = ""
    for x in glob.glob(config["neddata"] + "*"):
        if x.find(basename) != -1:
            file += x
    return file



'''
Runs the fastqc tool on all files in the cohort. Used the above function to find the files
'''
rule run_fastqc_ned:
    input:
        index = expand(config["bow_index"] + ".{ind}.bt2", ind=range(1,5)),
        indexrev = expand(config["bow_index"] + ".rev.{ind}.bt2", ind=range(1,3)),
        nl = lambda wildcards: nedfilefinder('{sample}'.format(sample=wildcards.sample))
    threads: 60
    log:
        join(config["loggingned"], "fastqc/{sample}.log")
    benchmark:
        join(config["benchned"], "fastqc/{sample}.txt")
    output:
        HTMLnl = temp(config["fastqcned"] + "{sample}_fastqc.html"),
        ZIPnl = temp(config["fastqcned"] + "{sample}_fastqc.zip")
    run:
         shell("fastqc {input.nl} -o {config[fastqcned]} --threads {threads} 2>> {log}")



'''
Runs the bowtie2 tool on the files. Once again uses the filefinder function to 
determine the file location.
'''
rule run_bowtie2_ned:
    input:
        HTMLnl = config["fastqcned"] + "{sample}_fastqc.html",
        ZIPnl = config["fastqcned"] + "{sample}_fastqc.zip",
        data = lambda wildcards: nedfilefinder('{basename}'.format(basename=wildcards.sample))
    output:
        temp(config["bowtie_result_ned"] + "{sample}.sam")
    threads: 60
    log:
        join(config["loggingned"], "bowtie2/{sample}.log")
    benchmark:
        join(config["benchned"], "bowtie2/{sample}.txt")
    run:
        shell("(bowtie2 -x {config[bow_index]} "
              "-U {input.data} "
              "-S {output} "
              "-p 60)"
              " 2> {log}")



'''
Runs the PicardCommandLine SortSam tool. Uses the output from the bowtie2 tool. 
Generated output is passed to the picard_fixmate rule.
'''

rule picard_sort_ned:
        input:
            config["bowtie_result_ned"] + "{sample}.sam"
        output:
            temp(config["picard_result"] + "{sample}.bam")
        threads: 60
        log:
            join(config["loggingned"], "picard/sort/{sample}.log")
        benchmark:
            join(config["benchned"], "picard/sort/{sample}.txt")
        run:
            shell("(PicardCommandLine SortSam INPUT={input} OUTPUT={output} SORT_ORDER=queryname) 2> {log}")




'''
Runs the PicardCommandLine FixMateInformation tool. Uses input from the picard_fixmate tool.
Generated output is passed to the picard_dupolicates rule.
'''
rule picard_fixmate_ned:
    input:
        config["picard_result"] + "{sample}.bam"
    output:
        temp(config["picard_fixmate"] + "{sample}.bam")
    log:
        join(config["loggingned"], "picard/fixmate/{sample}.log")
    benchmark:
        join(config["benchned"], "picard/fixmate/{sample}.txt")
    run:
        shell("(PicardCommandLine FixMateInformation INPUT={input} OUTPUT={output} SORT_ORDER=queryname) 2> {log}")




'''
Runs the PicardCommandLine MarkDuplicates tool. Uses input from the picard_fixmate rule.
Generated output is passed to the sam_sort rule.
'''
rule picard_duplicates_ned:
    input:
        config["picard_fixmate"] + "{sample}.bam"
    output:
        bam = temp(config["picard_duplicates"] + "{sample}.bam"),
        txt = temp(config["picard_duplicates"] + "{sample}.txt")
    log:
        join(config["loggingned"], "picard/markduplicates/{sample}.log")
    benchmark:
        join(config["benchned"], "picard/markduplicates/{sample}.txt")
    run:
        shell("(PicardCommandLine MarkDuplicates INPUT={input} OUTPUT={output.bam} METRICS_FILE={output.txt}) 2> {log}")



'''
Runs the samtools sort rule. Uses input from the picard_duplicates rule.
Generated output is passed to the feature_counts rule.
'''

rule sam_sort_ned:
    input:
        bam = config["picard_duplicates"] + "{sample}.bam",
        txt = config["picard_duplicates"] + "{sample}.txt"
    log:
        join(config["loggingned"], "samsort/{sample}.log")
    benchmark:
        join(config["benchned"], "samsort/{sample}.txt")
    output:
        temp(config["samsortned"] + "{sample}.bam")
    threads: 60
    run:
        shell("(samtools sort {input.bam} -o {output} --threads {threads}) 2> {log}")



'''
Runs the featureCounts rule. Uses input from the sam_sort rule.
Generated output is used  by the make_heatmap rule.
'''
rule feature_counts_ned:
    input:
        sam = expand(config["samsortned"] + "{sample}.bam", sample=samplesBasenameNed),
        gtf = config["gtf"]
    output:
        config["feature_ned"]
    log:
       join(config["loggingned"], "featurecount/featurelog.txt")
    benchmark:
        join(config["benchned"], "featurecounts/featurecount.txt")
    threads: 60
    shell:
         "(featureCounts -a {input.gtf} -o {output} {input.sam} -T {threads}) 2> {log}"


'''
Uses the input from the feature_counts to produce a heatmap.
It passes the input data to an existing R script. This script will use 20 genes
to create a heatmap and saves it in pdf format.
'''
rule make_heatmap_ned:
    input:
        config["feature_ned"]
    output:
        config["heatmapned"]
    shell:
        "Rscript HeatmapCreator.R {input} {output} {config[samsortned]}"
