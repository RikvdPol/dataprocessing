'''
Main script to be executed.
Includes all necessary Snakefiles needed
to execute the workflow.
'''

configfile: "config.yaml"
include: "SnakeNed.smk"
include: "SnakeBra.smk"




'''
Final rule of the scripts, recieves the output of the heatmap rules in de individual snakemake files.
'''
rule all:
    input:
         nl = expand(config["heatmapned"]),
         bra = expand(config["heatmapbra"])
    message:
        "created {input}"


'''
Build the indexes to be used in bowtie2, and creates a log. Since both cohort will uese
identical indexes, this rule is placed here.
'''
rule build_indexes:
    input:
        config["genome"]
    threads: 60
    log:
        join(config["bow_log"], "index.log")
    output:
        index = expand(config["bow_index"] + ".{ind}.bt2", ind=range(1,5)),
        indexrev = expand(config["bow_index"] + ".rev.{ind}.bt2", ind=range(1,3))
    shell:
        "(bowtie2-build {input} {config[bow_index]} --threads {threads}) 2>> {log}"