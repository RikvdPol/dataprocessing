configfile: "config.yaml"
import glob
import os
from os.path import join


samplesBra = [os.path.basename(x) for x in glob.glob(config["brazildata"] + "*")]
samplesBasenameBra = [x.split(".")[0] for x in samplesBra]

samplesBraBase = [x.rstrip("2_001_fastq.gz") for x in samplesBra if "_R2_" in x]

samplesR1 = [x for x in samplesBra if "R1" in x]
samplesR2 = [x for x in samplesBra if "R2" in x]

rule all_bra:
    input:
        expand(config["heatmapbra"])
    message:
        "created {input}"


'''
Build the indexes to be used in bowtie2
'''
# rule build_indexes:
#     input:
#         config["genome"]
#     threads: 80
#     log:
#         join(config["loggingbra"], "indexes/index.log")
#     output:
#         index = temp(expand(config["bow_index"] + ".{ind}.bt2", ind=range(1,5))),
#         indexrev = temp(expand(config["bow_index"] + ".rev.{ind}.bt2", ind=range(1,3)))
#     shell:
#         "(bowtie2-build {input} {config[bow_index]} --threads {threads}) 2> {log}"


def brafilefinder(basename):
    """
    Find the provided file in the filesystem
    """
    file = ""
    for x in glob.glob(config["brazildata"] + "*"):
        if x.find(basename) != -1:
            file += x
    return file




'''
Runs the fastqc tool on all files in the cohort. Used the above function to find the files
'''
rule run_fastqc_bra:
    input:
        index =  expand(config["bow_index"] + ".{ind}.bt2", ind=range(1,5)),
        indexrev = expand(config["bow_index"] + ".rev.{ind}.bt2", ind=range(1,3)),
        bra = lambda wildcards: brafilefinder('{sample}'.format(sample=wildcards.sample))
    threads: 60
    log:
        join(config["loggingbra"], "fastqc/{sample}.log")
    benchmark:
        join(config["benchbra"], "fastqc/{sample}.txt")
    output:
        HTMLbra = temp(config["fastqcbra"] + "{sample}_fastqc.html"),
        ZIPbra = temp(config["fastqcbra"] + "{sample}_fastqc.zip")
    shell:
         "fastqc {input.bra} -o {config[fastqcbra]} --threads {threads} 2>> {log}"



'''
This extra step is required because the Brazil cohort consists of paired data. Because of 
this the data has to be expanded in order to provide bowtie2 with both files.
'''
rule extra_step:
    input:
        expand(config["fastqcbra"] + "{sample}_fastqc.html", sample=samplesBasenameBra),
        expand(config["fastqcbra"] + "{sample}_fastqc.zip", sample=samplesBasenameBra)
    log:
        join(config["loggingbra"], "extrastep/extra.log")
    benchmark:
        join(config["benchbra"], "extrastep/extra.txt")
    output:
        temp(config["datadir"] + "test.txt")
    shell:
        "touch {config[datadir]}test.txt"



'''
Runs the bowtie2 tool. Fetches the R1 and R2 data using the base name. R1 and R2 are provided 
through manual means.
'''
rule run_bowtie2_bra:
    input:
        config["datadir"] + "test.txt",
        R1 = config["brazildata"] + "{samplebase}1_001.fastq.gz",
        R2 = config["brazildata"] + "{samplebase}2_001.fastq.gz"
    log:
        join(config["loggingbra"], "bowtie2/{samplebase}.log")
    benchmark:
        join(config["benchbra"], "bowtie2/{samplebase}.txt")
    output:
          temp(config["bowtie_result_bra"] + "{samplebase}.sam")
    run:
        shell("(bowtie2 -x {config[bow_index]} "
              "-1 {input.R1} "
              "-2 {input.R2} "
              "-S {output} "
              "-p 60)"
              " 2> {log}")



'''
Runs the PicardCommandLine SortSam tool. Uses the output from the bowtie2 tool. 
Generated output is passed to the picard_fixmate rule.
'''

rule picard_sort_bra:
        input:
           config["bowtie_result_bra"] + "{samplebase}.sam"
        output:
            temp(config["picard_bra"] + "{samplebase}_picresult.bam")
        threads: 80
        log:
            join(config["loggingbra"], "picard/sort/{samplebase}.log")
        benchmark:
            join(config["benchbra"], "picard/sort/{samplebase}.txt")
        run:
            shell("(PicardCommandLine SortSam INPUT={input} OUTPUT={output} SORT_ORDER=queryname) 2> {log}")


'''
Runs the PicardCommandLine FixMateInformation tool. Uses input from the picard_fixmate tool.
Generated output is passed to the picard_dupolicates rule.
'''
rule picard_fixmate_bra:
    input:
        config["picard_bra"] + "{samplebase}_picresult.bam"
    output:
        temp(config["picard_fix"] + "{samplebase}.bam")
    log:
        join(config["loggingbra"], "picard/fixmate/{samplebase}.log")
    benchmark:
            join(config["benchbra"], "picard/fixmate/{samplebase}.txt")
    run:
        shell("(PicardCommandLine FixMateInformation INPUT={input} OUTPUT={output} SORT_ORDER=queryname) 2> {log}")



'''
Runs the PicardCommandLine MarkDuplicates tool. Uses input from the picard_fixmate rule.
Generated output is passed to the sam_sort rule.
'''
rule picard_duplicates_bra:
    input:
        config["picard_fix"] + "{samplebase}.bam"
    output:
        bam = temp(config["picard_dups"] + "{samplebase}.bam"),
        txt = temp(config["picard_dups"] + "{samplebase}.txt")
    log:
        join(config["loggingbra"], "picard/markduplicates/{samplebase}.log")
    benchmark:
            join(config["benchbra"], "picard/markduplicates/{samplebase}.txt")
    run:
        shell("(PicardCommandLine MarkDuplicates INPUT={input} OUTPUT={output.bam} METRICS_FILE={output.txt}) 2> {log}")


'''
Runs the samtools sort rule. Uses input from the picard_duplicates rule.
Generated output is passed to the feature_counts rule.
'''
rule sam_sort_bra:
    input:
        bam = config["picard_dups"] + "{samplebase}.bam",
        txt = config["picard_dups"] + "{samplebase}.txt"
    output:
        temp(config["samsortbra"] + "{samplebase}.bam")
    log:
        join(config["loggingbra"], "samsort/{samplebase}.log")
    benchmark:
            join(config["benchbra"], "samsort/{samplebase}.txt")
    threads: 60
    run:
        shell("(samtools sort {input.bam} -o {output} --threads {threads}) 2> {log}")


'''
Runs the featureCounts rule. Uses input from the sam_sort rule.
Generated output is used  by the make_heatmap rule.
'''
rule feature_counts_bra:
    input:
        sam = expand(config["samsortbra"] + "{samplebase}.bam", samplebase=samplesBraBase),
        gtf = config["gtf"]
    output:
        config["feature_bra"]
    threads: 60
    log:
        join(config["loggingbra"], "featurecount/featurelog.txt")
    benchmark:
            join(config["benchbra"], "featurecounts/featurecount.txt")
    shell:
         "(featureCounts -a {input.gtf} -o {output} {input.sam} -T {threads}) 2> {log}"

'''
Uses the input from the feature_counts to produce a heatmap.
It passes the input data to an existing R script. This script will use 20 genes
to create a heatmap and saves it in pdf format.
'''
rule make_heatmap_bra:
    input:
        config["feature_bra"]
    output:
        config["heatmapbra"]
    shell:
        "Rscript HeatmapCreator.R {input} {output} {config[samsortbra]}"
