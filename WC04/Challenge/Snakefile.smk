rule all:
    input:
        "result/out.vcf"

rule bwa_index:
    input:
        'reference.fa'
    output:
        touch('bwa_index.done')
    shell:
        "bwa index {input}"

rule bwa_allign1:
    input:
        check = "bwa_index.done",
        gen = 'reference.fa',
        reads = "reads.txt"
    output:
        "temp/out.sai"
    shell:
        "bwa aln -I -t 8 {input.gen} {input.reads} > {output}"


rule bwa_allign2:
        input:
            gen = 'reference.fa',
            reads = "reads.txt",
            sai = "temp/out.sai"
        output:
              "alligned/out.sam"
        shell :
              "bwa samse {input.gen} {input.sai} {input.reads} > {output}"

rule sam_convert_to_bam:
    input:
        "alligned/out.sam"
    output:
        "temp/out.bam"
    shell:
         "samtools view -S -b {input} > {output}"

rule bam_sort:
    input:
         "temp/out.bam"
    output:
        "sorted/out.sorted.bam"
    shell:
        "samtools sort {input} -o {output}"


rule detect_remove_duplicates:
    input:
        "sorted/out.sorted.bam"
    output:
        "filtered/out.dedupe.bam"
    shell:
         "java -jar ~/picard/build/libs/picard.jar MarkDuplicates \
                            MAX_FILE_HANDLES_FOR_READ_ENDS_MAP=1000\
                            METRICS_FILE=out.metrics \
                            REMOVE_DUPLICATES=true \
                            ASSUME_SORTED=true  \
                            VALIDATION_STRINGENCY=LENIENT \
                            INPUT={input} \
                            OUTPUT={output}"
rule index_results:
    input:
        "filtered/out.dedupe.bam"
    output:
        touch("index.results.txt")
    shell:
         "samtools index {input}"

rule convert_to_bcf:
    input:
        fasta = "reference.fa",
        bam = "filtered/out.dedupe.bam",
        check = "index.results.txt"

    output:
        "result/out.vcf"
    shell:
         "samtools mpileup -uf {input.fasta} {input.bam} | bcftools view -> {output}"