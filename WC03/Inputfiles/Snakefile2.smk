rule bcftools_call:
    input:
        fa = "genome.fa",
        bam = expand(outdir + "sorted_reads/{sample}.bam", sample = config["samples"]),
        bai = expand(outdir + "sorted_reads/{sample}.bam.bai", sample = config["samples"])
    output:
        join(outdir,"calls/all.vcf")
    message:
        "executing samtools and bcftools on the following {input} to generate the following {output}."
    shell:
        "samtools mpileup -g -f {input.fa} {input.bam} | "
        "bcftools call -mv - > {output}"

rule report:
    input:
         join(outdir,"calls/all.vcf")
    output:
          join(outdir,"output.html")
    run:
        from snakemake.utils import report
        with open(input[0]) as f:
            n_calls = sum(1 for line in f if not line.startswith("#"))

        report("""
        An example workflow
        ===================================

        Reads were mapped to the Yeas reference genome 
        and variants were called jointly with
        SAMtools/BCFtools.

        This resulted in {n_calls} variants (see Table T1_).
        """, output[0], metadata="Author: Stijn Arends", T1=input[0])