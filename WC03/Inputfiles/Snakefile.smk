# -*- python -*-
from os.path import join
configfile: "config.yaml"

workdir: "/students/2019-2020/Thema11/Dataprocessing/WCO2/data/"
outdir = "/homes/rvandepol/Bachelor/Jaar_3/Thema11/dataprocessing/WC03/Inputfiles/"
include: "Snakefile2.smk"



rule all:
    input:
        join(outdir,"output.html")
        #expand('sorted_reads/{sample}.bam.bai', sample = SAMPLES),


rule bwa_map:
    input:
         genome =config["genome"] + config["ext"],
         sample ="samples/{sample}.fastq"
         #sample = "data/samples/{sample}.fastq"
         #"data/samples/A.fastq"
    output:
          join(outdir,"mapped_reads/{sample}.bam")
          #"mapped_reads/A.bam"
    benchmark:
        join(outdir,"benchmarks/{sample}.bwa.benchmark.txt")
    message:
        "executing bwa mem on the following {input} to generate the following {output}"
    shell:
         "bwa mem {input.genome} {input.sample} | samtools view -Sb - > {output}"
         #"bowtie2 -X {input.genome} -U {input.sample} -S {output}"

rule samtools_sort:
    input:
         join(outdir,"mapped_reads/{sample}.bam")
    output:
         join(outdir,"sorted_reads/{sample}.bam")
    message:
         "executing samtools sort on the following {input} to generate the following {output}."
    shell:
         "samtools sort -T sorted_reads/{wildcards.sample}"
         " -O bam {input} > {output}"

rule sametools_index:
    input:
         join(outdir, "sorted_reads/{sample}.bam")
    output:
          join(outdir,"sorted_reads/{sample}.bam.bai")
    message:
        "executing samtools index on the following {input} to generate the following {output}."
    shell:
         "samtools index {input}"

