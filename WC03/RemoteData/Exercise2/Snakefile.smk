from snakemake.remote.NCBI import RemoteProvider as NCBIRemoteProvider
NCBI = NCBIRemoteProvider(email="f.feenstra@pl.hanze.nl")

# get accessions for the first 3 results in a search for full-length Zika virus genomes
query = '"Zika virus"[Organism] AND (("9000"[SLEN] : "20000"[SLEN]) AND ("2017/03/20"[PDAT] : "2017/03/24"[PDAT])) '
accessions = NCBI.search(query, retmax=4)

input_files = expand("{acc}.fasta", acc=accessions)

rule all:
    input:
         "multiFastaZika.fasta"

rule createMultiFastaFile:
    input:
        NCBI.remote(input_files, db="nuccore", seq_start=5000)
    output:
        "multiFastaZika.fasta"
    run:
        shell("cat {input} >> {output}")
