import os
from snakemake.remote.NCBI import RemoteProvider as NCBIRemoteProvider
NCBI = NCBIRemoteProvider(email="f.feenstra@pl.hanze.nl") # email required by NCBI

rule all:
    input:
        NCBI.remote("KY785484.1.fasta", db="nuccore")
    run:
        outputName = os.path.basename("test.fasta")
        shell("mv {input} {outputName}")